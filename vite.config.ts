import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import {viteMockServe} from "vite-plugin-mock";

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        // viteMockServe({
        //     mockPath: "./mock/",
        //     enable: true,
        //     watchFiles: true
        // }),
    ],
    server: {
        cors: true,
        proxy: {
            "/manage": {
                target: "http://127.0.0.1:8001/",
                changeOrigin: false,
            }
        }
    }
    // server:{
    //   proxy:{
    //     "/api":{
    //       target: "http://localhost:8080/",
    //       changeOrigin: true
    //     }
    //   }
    // }
})
