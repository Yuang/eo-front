export default [
    {
        url: "/manage/mock/test",
        method: "get",
        response: () => {
            return {
                code: 0,
                message: "ok",
                data: "123"
            }
        }
    },
    // {
    //     url: "/manage/ItemInfo/Query",
    //     method: "post",
    //     response: () => {
    //         return {
    //             code: 0,
    //             message: "ok",
    //             data: {
    //                 page_size: 10,
    //                 page_num: 20,
    //                 content: [
    //                     {
    //                         name: "渡神级",
    //                         avatar: "/example_url",
    //                         detail: "渡神级描述",
    //                         type: "旗舰"
    //                     },
    //                 ]
    //             }
    //         }
    //     }
    // }
]