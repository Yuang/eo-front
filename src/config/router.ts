// @ts-ignore
import {createRouter, createRouterMatcher, createWebHashHistory} from "vue-router";
// import home from "../components/Home.vue";
import HomePage from "../page/HomePage.vue";
import UnFinishPage from "../page/UnFinishPage.vue";
import Foo from "../components/Foo.vue";
import LoginPage from "../page/LoginPage.vue";
import ItemInfoPage from "../page/ItemSearchPage.vue";
import TaskSearchPage from "../page/TaskSearchPage.vue";
import WHSearchPage from "../page/WHSearchPage.vue";
// const Foo = { template: '<div>foo</div>'}

const routes = [
    {
      path: "/",
      redirect: '/home'
    },
    {
        path: "/home",
        name: "home",
        component: HomePage
    },
    {
        path: "/ItemInfo",
        name: "ItemInfo",
        component: ItemInfoPage
    },
    {
        path: "/TaskQuery",
        name: "TaskInfo",
        component: TaskSearchPage
    },
    {
        path: "/WHQuery",
        name: "WHQuery",
        component: WHSearchPage
    },
    {
        path: '/foo',
        name: "foo",
        component: Foo
    },
    {
        path: '/Login',
        name: 'login',
        component: LoginPage
    },
    {
        path: '/unFinishPage',
        name: 'unFinishPage',
        component: UnFinishPage
    }
]

// @ts-ignore
export const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})