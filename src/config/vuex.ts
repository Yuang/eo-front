import {createStore} from "vuex";

export const store = createStore({
    state: {
        userInfo:{
            userID: '',
            userName: 'userName',
            cachedMenu: [
                {
                    path: "/Home",
                }
            ]
        }
    }
})