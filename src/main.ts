import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import {router} from "./config/router";
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css'
// @ts-ignore
import axios from 'axios';
// import VueAxios from 'vue-axios';/

createApp(App)
    .use(router)
    .use(ElementPlus)
    // .use(VueAxios, axios)
    // .use(VueCookies)
    .mount('#app')