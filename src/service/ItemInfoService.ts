import {Page, base_url, QueryParam} from "./common";
import axios, {AxiosResponse} from "axios";

export class ItemInfo {
    constructor(name: string, description: string, type: string) {
        this.name = name;
        this.description = description;
        this.type = type;
    }

    name: string;
    avatar: string | undefined;
    description: string;
    type: string;
}

export class ItemInfoQueryParam extends QueryParam{
    constructor() {
        super(10, 0);
    }
    ItemNameLike: string = "";
}

export const exampleRequest = function (): Promise<AxiosResponse<ItemInfo>> {
    return axios.post(
        "/"
    );
}

export const ItemInfoQueryRequest = function (): Promise<AxiosResponse<Page<ItemInfo>>> {
    return axios.post(
        base_url + "/ItemInfo/query"
    )
}
