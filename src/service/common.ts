export const base_url: string = "/manage"

export class Page<T> {
    constructor() {
        this.totalPages = 0;
        this.totalElements = 0;
        this.first = true;
        this.last = true;
        this.size = 0;
        this.number = 0;
        this.numberOfElements = 0;
        this.content = new Array<T>();
    }

    // page_size: number;
    //
    // page_num: number;
    totalPages: number;

    totalElements: number;

    first: boolean;

    last: boolean;

    size: number;

    number: number;

    sort: Array<string> | undefined;

    numberOfElements: number;

    content: Array<T>;
}

export class QueryParam {
    constructor(page_size:number, page_num:number) {
        this.page_size = page_size;
        this.page_num = page_num
    }
    page_size: number;
    page_num: number;
}


export interface Context<T, Q extends QueryParam> {
    res_page: Page<T>,
    param: Q,
    query: Function
}